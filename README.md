# Software used for making fpga

Here we don't develop the part of making a pcb, see my other project [Open Software]() to have more informations about this. (I don't publish this projet for now, so I didn't add link)

## Why we use software ?

We use software for each of the part of creating a solution in fpga :

```mermaid
flowchart TB;
    Y{Start};
    T[Find an idea];
    U[Find similars projects];
    V[Find ressources];
    W[Compile our work];

    A[Write];
    B[Analyse];
    C[Compile];
    D[Simulate];
    Dbis[View Simulations]
    E[Synthesis];
    F[Implementation];
    G[FPGA board];
    Gbis[FPGA tests];
    H[Type of project ?];
    Z{End - FPGA};
    
    J[Preparation];
    K[Synthesis];
    L[Placement];
    M[Static Timing Analysis];
    N[Routing];
    O[Migration];
    P[DRC];
    Q[LVS];
    R[GDS];
    S[View];
    Zbis{End - Silicon};

    subgraph Pre-project
      Y-->T;
      T-->U;
      T-->V;
      U-->W;
      V-->W;
    end

    subgraph FPGA developpement
    direction TB;
    W-->A;
    A-->B;
    B-->C;
    C-- Recommended -->D;
    C-- Not Recommended -->E;
    D-->Dbis;
    Dbis-- Pass -->E;
    Dbis-- Fail -->A;
    E-->F;
    F-->G;
    G-->Gbis;
    Gbis-- Pass -->H;
    Gbis-- Fail -->A;
    H-- FPGA -->Z;
    end;

    subgraph Silicon developpement
    direction TB;

    H-- Silicon -->J;
    J --> K;
    K --> L;
    L --> M;
    M --> N;
    N --> O;
    O --> P;
    P --> Q;
    Q --> R;
    R -- Fail --> J;
    R -- Pass --> Zbis;
    Zbis -- Optional --> S;
    end;
```

So now we can replace function by software to find what we need :

-- I need to make a graph here --



So, with this graph we can now see wich sofware we need.



For those how want, here is a list of all free and open-source software you can use :

- IDE :
  
  - VS Codium : open source core of VS Code without all the telemetric part
  
  - VIM : A powerfull IDE that I personnaly don't use.

- FPGA part :
  
  - GHDL : toolchain to simulate and implemente VHDL code



The companies also provide complete solution, but it close source. However, sometimes you need to use this software to synthesis and implement for design. So, here is some of this solutions :

- Vivado by Xilinx / AMD

- Quartus by Intel

- Gowin EDA
